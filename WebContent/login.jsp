<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>

   <head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

		<!-- DANEDAN animation-->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

		<!-- My CSS style -->
		<link rel="stylesheet" href="rwa.css">

		
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
				
		
		<title>VVC</title>

	</head>

	<body>

		<!-- START Navigation bar -->
		<div class="container-fluid fixed-top">
			<nav class="navbar d-flex glossy-black-bg mx-n3">


				<!-- Logo or name for the page -->
				<a class="navbar-brand navifont black-glossy-navbar-font" href="#">
					<i class="fa fa-play navbar-fs" aria-hidden="true"></i>
					<span class="navbar-fs">VIDEO VOTING COMPETITION</span>
				</a>


				<div class="d-flex flex-grow-1 text-right">
					<!-- ml-auto: left margin auto to shift elements to the right of navigation bar -->
					<!-- flex-nowrap: all elements will be in one row -->
					<!-- navbar-nav: parent class to nav list -->
					<ul class="navbar-nav ml-auto flex-nowrap flex-row">

						<!-- nav-item: class for nav-item -->
						<li class="nav-item d-flex align-middle pt-1">
							<!-- Font Awesome class for icons -->
							<a href="index.jsp" class="d-flex align-middle">
								<span class="fas fa-home fa-lg px-3 navicon navbar-fs"></span>
							</a>
						</li>


						<!-- nav-item: class for nav-item -->
						<li class="nav-item d-flex align-middle pt-1">
							<!-- Font Awesome class for icons -->
							<a href="ranking.jsp" class="d-flex align-middle">
								<span class="fas fa-bars fa-lg px-3 navicon navbar-fs"></span>
							</a>
						</li>

					</ul>
				</div>
			</nav>
		</div>
		<!-- END Navigation bar -->



		<!-- SPACE V3 BEGIN -->
		<div class="container-fluid">
			<div class="row text-center py-2 py-sm-2 py-md-4 py-lg-4 py-xl-5">
				<div class="col-12"></div>
			</div>
		</div>
		<!-- SPACE END -->


		
		<!-- SPACE V2 BEGIN -->
		<div class="container-fluid">
			<div class="row text-center py-2 py-sm-4 py-md-4 py-lg-4 py-xl-5">
				<div class="col-12"></div>
			</div>
		</div>
		<!-- SPACE END -->


		<!-- SPACE V1 BEGIN -->
		<div class="container-fluid">
			<div class="row text-center py-3 py-md-4 py-xl-5">
				<div class="col-12"></div>
			</div>
		</div>
		<!-- SPACE END -->
		

		
		<!-- VVC LOGO START -->
		<div class="container-fluid">
			<div class="row text-center">
			
				<div class="col-3"></div>
				
				<div class="col-6">
					<i class="fas fa-user logo-fs" aria-hidden="true"></i>
				</div>
				
				<div class="col-3"></div>
			
			</div>
		</div>
		<!-- VVC LOGO END -->


		
		<!-- SPACE BEGIN -->
		<div class="container-fluid">
			<div class="row text-center py-1 py-sm-2 py-md-2 py-lg-3">
				<div class="col-12"></div>
			</div>
		</div>
		<!-- SPACE END -->





		<!-- LOGIN -->
		<div class="container">
			<div class="row">
				<div class="col-3 col-md-3 col-lg-4"></div>
		
				<div class="col-6 col-md-6 col-lg-4" id="login">

					<form action="Login" method="post">  
						<input type="text" name="username" class="input-dim" placeholder="Username"/><br/>  
						<input type="password" name="password" class="input-dim" placeholder="Password"/><br/>  
						<input type="submit" class="submit-dim" value="LOGIN">  
					</form>  
			
				</div>
				
				<div class="col-3 col-md-3 col-lg-4"></div>
			</div>
		</div>
		<!-- END LOGIN -->


		
		<!-- SPACE V1 BEGIN -->
		<div class="container-fluid">
			<div class="row text-center py-3 py-md-4 py-xl-5">
				<div class="col-12"></div>
			</div>
		</div>
		<!-- SPACE END -->
		
		
		<!-- SPACE V2 BEGIN -->
		<div class="container-fluid">
			<div class="row text-center py-2 py-sm-4 py-md-4 py-lg-4 py-xl-5">
				<div class="col-12"></div>
			</div>
		</div>
		<!-- SPACE END -->


		
		<!-- SPACE V3 BEGIN -->
		<div class="container-fluid">
			<div class="row text-center py-2 py-sm-2 py-md-4 py-lg-4 py-xl-5">
				<div class="col-12"></div>
			</div>
		</div>
		<!-- SPACE END -->
    
		<div class="container-fluid">
			<div class="row glossy-black-bg text-center py-1 py-sm-2 py-md-3 py-lg-4">
				<div class="col-3"></div>
				<div class="col-6">
					<a class="h1 fet-logo-style" href="https://fet.ba">
						FET
					</a>
				</div>
				<div class="col-3"></div>
			</div>
		</div>

    
	</body>

</html>
