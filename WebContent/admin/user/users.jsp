<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>



<!DOCTYPE html>
<html>

   <head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

		<!-- DANEDAN animation-->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

		<!-- My CSS style -->
		<link rel="stylesheet" href="admin.css">

		<!--  
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		-->
		 <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
				
		
		<title>VVC</title>

	</head>

	<body>

		<!-- START Navigation bar -->
		<div class="container-fluid fixed-top">
			<nav class="navbar d-flex glossy-black-bg mx-n3">


				<!-- Logo or name for the page -->
				<a class="navbar-brand navifont black-glossy-navbar-font" href="../">
					<i class="fa fa-play navbar-fs" aria-hidden="true"></i>
					<span class="navbar-fs">VIDEO VOTING COMPETITION</span>
				</a>


				<div class="d-flex flex-grow-1 text-right">
					<!-- ml-auto: left margin auto to shift elements to the right of navigation bar -->
					<!-- flex-nowrap: all elements will be in one row -->
					<!-- navbar-nav: parent class to nav list -->
					<ul class="navbar-nav ml-auto flex-nowrap flex-row">



						<!-- nav-item: class for nav-item -->
						<li class="nav-item d-flex align-middle pt-1">
							<!-- Font Awesome class for icons -->
							<a href="index.jsp" class="d-flex align-middle">
								<span class="fas fa-edit fa-lg px-3 navicon navbar-fs"></span>
							</a>
						</li>


						<!-- nav-item: class for nav-item -->
						<li class="nav-item d-flex align-middle pt-1">
							<!-- Font Awesome class for icons -->
							<a href="../index.jsp" class="d-flex align-middle">
								<span class="fas fa-home fa-lg px-3 navicon navbar-fs"></span>
							</a>
						</li>


						<!-- nav-item: class for nav-item -->
						<li class="nav-item d-flex align-middle pt-1">
							<!-- Font Awesome class for icons -->
							<a href="../ranking.jsp" class="d-flex align-middle">
								<span class="fas fa-bars fa-lg px-3 navicon navbar-fs"></span>
							</a>
						</li>
						
						
						<!-- nav-item: class for nav-item -->
						<li class="nav-item d-flex align-middle pt-1">
							<!-- Font Awesome class for icons -->
							<a href="../Logout" class="d-flex align-middle">
								<span class="fas fa-times-circle fa-lg px-3 navicon navbar-fs"></span>
							</a>
						</li>
						

					</ul>
				</div>
			</nav>
		</div>
		<!-- END Navigation bar -->


	   <!-- 
        <form id="post-form" name="post-form" action="" method="POST" enctype="multipart/form-data">
        	<input type="file" name="file" />
        	<input type="hidden" name="youtubeVideoID" value="quxTnEEETbo"/>
            <input type="submit" value="upload"/>
        </form>   
        -->
        
        <!-- 
        <form name="post_form" id="post_form" action="" method="POST">
        	<input type="hidden" name="post_type" value="post" />
             <input type="text" name="userpost"/>
             <input type="submit" name="post_submit" value="post" id="post_submit_button"/>
        </form>
        -->
        <!--  
         <form action="UpdateVideoTitle" method="post" enctype="multipart/form-data"  onsubmit="return false">
        	<input type="text" name="title" />
        	<input type="hidden" name="videoYoutubeID" value="quxTnEEETbo"/>
             <button type='button'>submit</button> 
            <input type="submit" value="upload" />
        </form>   
		-->
    
  
    
    
    
    
    
    
    
    
  
  
    
    
    
    
    
		<!--  CHANGE PASSWORD MODAL START -->
		<div id="changePasswordModal" class="black-glossy-modal">
					
			<div class="container-fluid content">
					
					
						
				<div class="row header text-center black-glossy-sfonts py-2">
					<div class="col-1 py-1 px-0"></div>
					<div class="col-10 py-1 px-0">
						<!--  FA IMAGE  -->
						<span class="fas fa-key fa-lg"></span>
					</div>
					<div class="col-1 py-1 px-0" style="font-size: 1.2vw;" onclick="displayNoneChangePasswordModal();">
						<i class="fa fa-times-circle navicon" aria-hidden="true"></i>
					</div>
				</div>
					
					
					
				<div class="row body text-center black-glossy-copylink-font pt-3 pb-0">
					<div class="col-1 px-0"></div>

					<div class="col-10 px-0">
						<div class="row align-middle mx-0">
							
								
							<div class="col-1 px-0"></div>
								
							<div class="col-10 px-0">
								<form id="change-password-form" name="change-password-form" action="" method="POST" enctype="multipart/form-data">
									
									
									<div class="row">

										<div class="col-1"></div>
									
										<div class="col-10">
											<input type="text" name="password" placeholder="PASSWORD"/>
										</div>
										
										<div class="col-1"></div>
							
									</div>
							
							
        							<div class="row">
										<div class="col-12">
											<input type="submit" value="UPLOAD" onclick="displayNoneChangePasswordModal();"/>
										</div>
									</div>
									
        							
        							<input type="hidden" class="hiddenUsername" name="username" value=""/>
        							
        						
        						</form>   
							</div>
							
							<div class="col-1 px-0"></div>
							
							
							<!--  
							<div class="col-2 .special-font tooltip1 my-auto px-0">
  								<span id="imageModal-submit" style="font-size:1.02vw;" 
  									   onclick="submitImageModal(); displayNoneImageModal();"
  								>
  									<i class="fas fa-check-circle fa-lg px-3 navbar-fs navicon-green"></i> 
  								</span>
							</div>
							-->		

			
						</div>
					</div>

					<div class="col-1 px-0"></div>
								
								
								
				</div>
					
					
			</div>
				
		</div>
		<!-- PASSWORD MODAL END -->   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    	<!--  ADD USER MODAL START -->
		<div id="addUserModal" class="black-glossy-modal">
					
			<div class="container-fluid content">
					
					
						
				<div class="row header text-center black-glossy-sfonts py-2">
					<div class="col-1 py-1 px-0"></div>
					<div class="col-10 py-1 px-0">
						<!--  FA IMAGE  -->
						<span class="fas fa-users fa-lg"></span>
					</div>
					<div class="col-1 py-1 px-0" style="font-size: 1.2vw;" onclick="displayNoneAddUserModal();">
						<i class="fa fa-times-circle navicon" aria-hidden="true"></i>
					</div>
				</div>
					
					
					
				<div class="row body text-center black-glossy-copylink-font pt-3 pb-0">
					<div class="col-1 px-0"></div>

					<div class="col-10 px-0">
						<div class="row align-middle mx-0">
							
								
							<div class="col-1 px-0"></div>
								
							<div class="col-10 px-0">
								<form id="add-user-form" name="add-user-form" action="" method="POST" enctype="multipart/form-data">
							
									
									<div class="row">
										<div class="col-6">
											<input type="text" name="username" placeholder="USERNAME"/>
										</div>
										
										<div class="col-6">
											<input type="text" name="password" placeholder="PASSWORD"/>								
										</div>
									</div>
							
							
        							<div class="row">
										<div class="col-12">
											<input type="submit" value="UPLOAD" onclick="displayNoneAddUserModal();"/>
										</div>
									</div>
        							
        							
        						</form>   
							</div>
							
							<div class="col-1 px-0"></div>
							

			
						</div>
					</div>

					<div class="col-1 px-0"></div>
								
								
								
				</div>
					
				<div class="row footer text-center m-n2 m-md-0 py-0">
					<div class="col-12">&nbsp</div>
				</div>
					
			</div>
				
		</div>
		<!-- ADD USER MODAL END -->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    	<!--  ADD USER ROLE MODAL START -->
		<div id="addUserRoleModal" class="black-glossy-modal">
					
			<div class="container-fluid content">
					
					
						
				<div class="row header text-center black-glossy-sfonts pt-3 pb-0">
					<div class="col-1 py-1 px-0"></div>
					<div class="col-10 py-1 px-0">
						<!--  FA IMAGE  -->
						<span class="fas fa-user-plus fa-lg"></span>
					</div>
					<div class="col-1 py-1 px-0" style="font-size: 1.2vw;" onclick="displayNoneAddUserRoleModal();">
						<i class="fa fa-times-circle navicon" aria-hidden="true"></i>
					</div>
				</div>
					
					
					
				<div class="row body text-center black-glossy-copylink-font py-3">
					<div class="col-1 px-0"></div>

					<div class="col-10 px-0">
						<div class="row align-middle mx-0">
							
								
							<div class="col-1 px-0"></div>
								
							<div class="col-10 px-0">
								<form id="add-user-role-form" name="add-user-role-form" action="" method="POST" enctype="multipart/form-data">
									
									<input type="text" name="roleTitle" placeholder="ROLE"/>
									
									<input type="hidden" class="hiddenUsername" name="username" value=""/>
									
        							<div class="row">
										<div class="col-12">
											<input type="submit" value="UPLOAD" onclick="displayNoneAddUserRoleModal();"/>
										</div>
									</div>
        							
        							
        						</form>   
							</div>
							
							<div class="col-1 px-0"></div>
							
							
							<!--  
							<div class="col-2 .special-font tooltip1 my-auto px-0">
  								<span id="imageModal-submit" style="font-size:1.02vw;" 
  									   onclick="submitImageModal(); displayNoneImageModal();"
  								>
  									<i class="fas fa-check-circle fa-lg px-3 navbar-fs navicon-green"></i> 
  								</span>
							</div>
							-->		

			
						</div>
					</div>

					<div class="col-1 px-0"></div>
								
								
								
				</div>
					
			</div>
				
		</div>
		<!-- ADD USER ROLE MODAL END -->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


		<div class="container-fluid">
			<div class="row justify-content-center py-3 py-md-4 py-lg-5">
				<div class="col-6 text-center">

					<div class="row pb-3">
						<div class="col-12">
							<span class="fas fa-users fa-fs" onclick="displayBlockAddUserModal()"></span>
						<!-- <span class="far fa-edit fa-fs"></span> --> 
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<!-- <h1 class="animated infinite bounce delay-2s">Example</h1> -->
							<a class="h1 animated pulse special-anim ranking-pg-font black-font" href="#rankings">USER PANEL</a>
							<!-- <a class="h2 special-font animated infinite bounce delay-2s" href="#top5">TOP 5</a> -->
						</div>
					</div>

				</div>
			</div>
		</div>





				<!-- TEMPLATE ROLE ELEMENT START -->
				<div class="col-12 justify-content-center align-items-center py-3 notdisplay" id="user-role-template">
					<div class="row black-glossy-title min-height-35 align-middle w-100">
						<div class="col-12 justify-content-center align-items-center align-middle title role">
							ROLE
						</div>
					</div>
				</div>
				<!-- TEMPLATE ROLE ELEMENT END -->








				<!-- TEMPLATE ELEMENT START -->
				<div class="row py-4 py-lg-5 notdisplay" class="video-row-template user-template" id="user-template" username="">
				<div class="col-12">
				
					<div class="row mb-n3" style="z-index: 100;">
						<div class="col-5"></div>
						<div class="col-2 black-glossy-rank text-center delete-video-style" username="" onclick="deleteUser(this)">
							<i class="fa fa-trash" aria-hidden="true"></i>
						</div>
						<div class="col-5"></div>
					</div>
	
					
					<div class="row d-flex black-glossy py-3 py-md-1_5 py-lg-2 title-margin">
						
						<div class="col-1"></div>
						
						<div class="col-3 d-flex justify-content-center align-items-center py-2">
							<div class="row black-glossy-title min-height-35 align-middle">
								<div class="col-12 title username" username="" onclick="displayBlockChangePasswordModal(this);">
									USERNAME
								</div>
							</div>
						</div>
						
						<div class="col-3"></div>
						
						<div class="col-1 py-2 my-auto">
							<!-- <span class="fas fa-users" onclick="displayBlockInsertVideoModal()"></span> -->
							<i class="fas fa-user-plus addUserBtn" username="" onclick="displayAddUserRoleModal(this)"></i>
						</div>
			
						
						<div class="col-3 my-auto">
							<div class="row roleList" onclick="displayNextRole(this);">
								
								<!--  PLACE WHERE USER ROLES WILL BE ADDED -->
								
							</div>
						</div>
						
						<div class="col-1 py-2 my-auto">
							<span class="far fa-trash-alt navicon removeUserBtn" username="" onclick="deleteUserRole(this)"></span>
							<!-- <span class="fas fa-users" onclick="displayBlockInsertVideoModal()"></span> -->
						</div>
																	
					</div>
				

				</div>
			</div>
			<!-- TEMPLATE ELEMENT END -->





		<!-- START RANKING LIST -->
		<div class="container p1-2 pb-4 pt-md-3 pb-md-5">
		<div class="row">
		<div class="col-1 col-sm-0"></div>
		<div class="col-10 col-sm-12" id="userList"
		<% 
	 			String pageNumber = request.getParameter("pageNumber");
	 			
	 			if(pageNumber == null)
	 			{
	 				out.println("pageNumber=\"1\"");	
	 			}
	 			else
	 			{
	 				try{
	 					Integer pn = Integer.parseInt(pageNumber);
	 					out.println("pageNumber=\"" + pn + "\"");
	 				}
	 				catch(Exception e)
	 				{
	 					out.println("pageNumber=\"1\"");
	 				}
	 			}
	 		%>
	 	>


			<!--  THERE WILL BE ELEMENTS -->



		</div>
		<div class="col-1 col-sm-0"></div>
		</div>
		</div>
		<!-- END RANKINGS LIST -->





		<!-- PAGINATION MY -->
		<div class="container-fluid">
			<div class="row justify-content-center pt-2 pt-md-3 pt-lg-4 pb-3 pb-md-4 pb-lg-5">

				<div class="col-1 col-md-2">
					<!--  
					<a href="#rankings" class="black-font">
						<span class="fas fa-arrow-left fa-fs"></span>
					</a>
					-->
				</div>

				<div class="col-10 col-md-8">
					<!-- PAGINATION -->
					<nav>
  
  						<ul class="pagination justify-content-center"
  							startPage="1"
  							databaseVideoCount="10"
  							style="font-size:1.02vw">
    						
    						<li class="page-item disabled" id="startPageBtn" onclick="firstPage()">
    							<a class="page-link">
									<i class="fas fa-fast-backward"></i>
								</a>
							</li>
							
							
							<li class="page-item disabled" id="prevPageBtn" onclick="prevPage()">
    							<a class="page-link">
									<i class="fas fa-step-backward"></i>
								</a>
							</li>
							
					
   			
    			
    						<li class="page-item active" id="pg1" onclick="thisPage(this)"><a class="page-link">1</a></li>
    						<li class="page-item" id="pg2" onclick="thisPage(this)"><a class="page-link">2</a></li>
    						<li class="page-item" id="pg3" onclick="thisPage(this)"><a class="page-link">3</a></li>
    						<li class="page-item" id="pg4" onclick="thisPage(this)"><a class="page-link">4</a></li>
    						<li class="page-item" id="pg5" onclick="thisPage(this)"><a class="page-link">5</a></li>
    						
    						
    						<li class="page-item" id="nextPageBtn" onclick="nextPage()">
    							<a class="page-link">
									<i class="fas fa-step-forward"></i>
								</a>
    						
    			
    						<li class="page-item" id="lastPageBtn" onclick="lastPage()">
    							<a class="page-link">
									<i class="fas fa-fast-forward"></i>
    							</a>
    						</li>
 		 				
 		 				</ul>
					</nav>
					<!-- END PAGINATION -->
				
				
				</div>

				<div class="col-1 col-md-2">
					<!-- 
					<a href="#rankings" class="black-font">
						<span class="fas fa-arrow-right fa-fs"></span>
					</a>
					-->
				</div>

			</div>

		</div>
		<!-- END MY PAGINATION -->
		
		
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
		<div class="container-fluid">
			<div class="row glossy-black-bg text-center py-1 py-sm-2 py-md-3 py-lg-4">
				<div class="col-3"></div>
				<div class="col-6">
					<a class="h1 fet-logo-style" href="https://fet.ba">
						FET
					</a>
				</div>
				<div class="col-3"></div>
			</div>
		</div>

   	</body>
   	
   	<script>
   		$("#add-user-role-form").submit(function (event) {
   	   		console.log("SUBMIT ADD USER ROLE");
   		    event.preventDefault();
   		   
   		    //var hiddenYTVideoIDDiv = this.getElementsByClassName("hiddenVideoID");
			//var youtubeVideoID =  hiddenYTVideoIDDiv[0].getAttribute("value");
			//console.log("SUBMIT YT VIDEO ID: " + youtubeVideoID);
			
   		    
   		    var formData = new FormData(this);
   			console.log(formData);

   			var username = formData.get("username");
   			console.log(username);

   			var roleTitle = formData.get("roleTitle");
   			console.log(roleTitle);
   			
   			var formDataSerialized = $(this).serialize();
   			console.log(formDataSerialized);

   		    $.ajax({
   		        url: 'user/role?' + formDataSerialized,
   		        type: 'POST',
   		        data: null,
   		        async: false,
   		        cache: false,
   		        contentType: false,
   		        processData: false,
   		        success: function (returndata) {
   		            //$("#productFormOutput").html(returndata);
   		            alert("Successful upload");
   		            addRole(username, roleTitle);
   		        },
   		        error: function () {
   		            alert("error in ajax form submission");
   		        }
   		    });

   		    return false;
   		});



   		
   		$("#add-user-form").submit(function (event) {
   	   		console.log("SUBMIT ADD USER FORM");
   		    event.preventDefault();
   		    
   		    var formData = new FormData(this);
   			console.log(formData);

   			var formDataSerialized = $(this).serialize();
   		    console.log(formDataSerialized);
   
   		    $.ajax({
   		        url: 'user?' + formDataSerialized,
   		        type: 'POST',
   		        data: null,
   		        async: false,
   		        cache: false,
   		        contentType: false,
   		        processData: false,
   		        success: function (returndata) {
   		            //$("#productFormOutput").html(returndata);
   		            alert("Successful upload");
   		            clearUserList();
   		  			generateUserList();
   		        },
   		        error: function () {
   		            alert("error in ajax form submission");
   		        }
   		    });

   		    return false;
   		});


   		$("#change-password-form").submit(function (event) {
   	   		console.log("SUBMIT CHANGE PASSWORD FORM");
   		    event.preventDefault();
   		    
   		    var formData = new FormData(this);
   			console.log(formData);

   			var formDataSerialized = $(this).serialize();
   		    console.log(formDataSerialized);

   		    $.ajax({
   		        url: 'user/password',
   		        type: 'POST',
   		        data: formData,
   		        async: false,
   		        cache: false,
   		        contentType: false,
   		        processData: false,
   		        success: function (returndata) {
   		            //$("#productFormOutput").html(returndata);
   		            alert("Successful upload");
   		        },
   		        error: function () {
   		            alert("error in ajax form submission");
   		        }
   		    });

   		    return false;
   		});
   	</script>
   	
   	
   	<!-- MY JAVASCRIPT-->
    <script type="text/javascript" src="communicationUser.js"></script>
    <script type="text/javascript" src="animationUser.js"></script>

	<script>
		handleClientUserLoad();
	</script>


</html>