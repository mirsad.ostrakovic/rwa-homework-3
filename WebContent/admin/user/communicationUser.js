var APIkey = "AIzaSyD-8Rv2NLpVsceAHSQ7kwwV3oAzDUNkUlA";


function fetchJSONFile(path, callback) {
	var httpRequest = new XMLHttpRequest();
	
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200) {
				var data = JSON.parse(httpRequest.responseText);
				if (callback) callback(data);
			}
		}
	};
	
	httpRequest.open('GET', path);
	httpRequest.send(); 
} 


function getUserCountDB() {
	 
	 URL = "http://localhost:8080/RWA_HOMEWORK_2/admin/GetUserCount";
	 
	 fetchJSONFile(URL, function(data) {
		 
		 console.log("USER COUNT GET");
		 
		 var paginationDiv = document.getElementsByClassName("pagination")[0];
		 paginationDiv.setAttribute("databaseUserCount", data);
		
		 firstPage();
		 console.log("Database video count: ", data);
	 });
}

function clearUserList() {

	console.log("CLEAR USER LIST")
	
	var userList = document.getElementById("userList");

	while (userList.firstChild) {
		userList.removeChild(userList.firstChild);
	}
}
 



function generateUserList() {
	
 	var userTemplate = document.getElementById("user-template");
	var userList = document.getElementById("userList");
	var pageNumber = userList.getAttribute("pageNumber");
	var URL = "http://localhost:8080/RWA_HOMEWORK_2/admin/GetUsers?startPosition=" + (pageNumber-1) * 10 + "&maxUsersCount=10";
	var roleTemplate = document.getElementById("user-role-template");
	
	
	console.log("GENERATE USER LIST")
	
	fetchJSONFile(URL, function(data) {
				 
		var usersList = data;
		
		for(var i = 0; i < usersList.length; ++i)
		{
			
			var user = data[i];
			var roles = user.userRolesCollection;
			
			var newUserListItem = userTemplate.cloneNode("true");
			
			newUserListItem.classList.remove("notdisplay");		
			
			newUserListItem.setAttribute("username", user.username);
			
			newUserListItem.getElementsByClassName("username")[0].textContent = user.username;
			newUserListItem.getElementsByClassName("username")[0].setAttribute("username", user.username);
			
			newUserListItem.getElementsByClassName("delete-video-style")[0].setAttribute("username", user.username);
			
			
			newUserListItem.getElementsByClassName("addUserBtn")[0].setAttribute("username", user.username);
			newUserListItem.getElementsByClassName("removeUserBtn")[0].setAttribute("username", user.username);
			
			var roleList = newUserListItem.getElementsByClassName("roleList")[0]
			
			for(var j = 0; j < roles.length; ++j)
			{
				var newRoleItem = roleTemplate.cloneNode("true");
				newRoleItem.getElementsByClassName("role")[0].textContent = roles[j].roleTitle;
				roleList.appendChild(newRoleItem);
			}
			
			if(roles.length != 0)
			{
				roleList.children[0].classList.add("d-flex");
				roleList.children[0].classList.remove("notdisplay");
			}
				
			userList.appendChild(newUserListItem);
		}
		
	
			
					
				//newRankingListItem.getElementsByClassName("black-glossy-video-stat")[0].setAttribute("rating", Math.round(video.videoScore * 100));		 
				
				//newRankingListItem.getElementsByClassName("title")[0].textContent = title;//title;
				
				// for administration purpose
				
				//newRankingListItem.getElementsByClassName("circle-img-div")[0].setAttribute("youtubeVideoID", video.videoYoutubeID);
				
				//newRankingListItem.getElementsByClassName("title")[0].setAttribute("youtubeVideoID", video.videoYoutubeID);
				
				//newRankingListItem.getElementsByClassName("delete-video-style")[0].setAttribute("youtubeVideoID", video.videoYoutubeID);
				
				
				
				//if(video.hasVideoImage)
				//	newRankingListItem.getElementsByClassName("round-video-img")[0].setAttribute("src", "../GetImage?id=" + video.videoYoutubeID);
				//else
				//	newRankingListItem.getElementsByClassName("round-video-img")[0].setAttribute("src", "https://img.youtube.com/vi/" + video.videoYoutubeID + "/hqdefault.jpg");
						 
				//newRankingListItem.getElementsByClassName("video-stat-text")[0].textContent = video.videoLikes + "/" + video.videoDislikes;
			
				//newRankingListItem.getElementsByClassName("black-glossy-rank")[0].textContent = (pageNumber-1) * 10 + i + 1;
						 
				//rankingList.appendChild(newRankingListItem);
					 				 
});
}



function handleClientUserLoad() {
	console.log("INIT")
	initClientUserLoad();	
}

function initClientUserLoad() {
	getUserCountDB();
	generateUserList();
}





function deleteUserRole(deleteRoleDiv)
{
	var username = deleteRoleDiv.getAttribute("username");
	var roleTitle = null; 
	
	var parentDiv = deleteRoleDiv.parentNode.parentNode;
	
	var rolesDivs = parentDiv.getElementsByClassName("roleList")[0].children;

	
	for(var i = 0; i < rolesDivs.length; ++i)
	{
		if(!rolesDivs[i].classList.contains("notdisplay"))
		{
			console.log(rolesDivs[i]);
			roleTitle = rolesDivs[i].getElementsByClassName("role")[0].textContent
			break;
		}
	}
	
	if(roleTitle == null && username == null)
	{
		console.log("addUserRole(): Error happened");
		return;
	}
	

	$.ajax({
	    url: 'role?username=' + username + "&roleTitle=" + roleTitle,
	    type: 'DELETE',
	    data: null,
	    async: false,
	    cache: false,
	    contentType: false,
        processData: false,
	    success: function (returndata) {
	    	alert("Successful upload");
	    	deleteRole(username, roleTitle);
	    },
	    error: function () {
	    	alert("error in ajax form submission");
	    }
	});
}


function deleteUser(deleteUserDiv) {
	var username = deleteUserDiv.getAttribute("username");
	
	$.ajax({
			url: '../user?username=' + username,
			type: 'DELETE',
			data:  null,
			async: false,
			cache: false,
			contentType: false,
			processData: false,
			success: function (returndata) {
				//$("#productFormOutput").html(returndata);
				alert("Successful delete");
				clearUserList();
				generateUserList();
			},
	        error: function () {
	            alert("error in ajax form submission");
	        }
	});
}
