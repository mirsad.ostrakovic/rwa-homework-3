function videoStatEnterAnimation(this_element) {
	var state = this_element.getAttribute("state");
	var rating = this_element.getAttribute("rating");
				
	var leftFillBar = this_element.getElementsByClassName("black-glossy-video-stat-left-bar")[0];
	var rightFillBar = this_element.getElementsByClassName("black-glossy-video-stat-right-bar")[0];
		 
	this_element.setAttribute("state", "enter");
					
	var leftFillBarWidth = parseInt(leftFillBar.style.width);
	var rightFillBarWidth = parseInt(rightFillBar.style.width);

	var width = leftFillBarWidth;

	var id = setInterval(frame, 6);
	
	function frame() {
		if (this_element.getAttribute("width") > 100 || this_element.getAttribute("state") != "enter") 
		{
			clearInterval(id);
		} 
		else 
		{
			var width = this_element.getAttribute("width");
			width++;	
			this_element.setAttribute("width", width);
			leftFillBar.style.width = (width * rating / 100 + 3) + '%';
			rightFillBar.style.width = (width * (100 - rating) / 100 + 3) + '%';
			rightFillBar.style.left = (100 - width * (100 - rating) / 100) + '%'; 
		}
	}
}













function videoStatLeaveAnimation(this_element) {
	var state = this_element.getAttribute("state");
	var rating = this_element.getAttribute("rating");

				
	var leftFillBar = this_element.getElementsByClassName("black-glossy-video-stat-left-bar")[0];
	var rightFillBar = this_element.getElementsByClassName("black-glossy-video-stat-right-bar")[0];
		 
	this_element.setAttribute("state", "leave");
				
			
	var leftFillBarWidth = parseInt(leftFillBar.style.width);
	var rightFillBarWidth = parseInt(rightFillBar.style.width);

				
	var id = setInterval(frame, 8);
			
	function frame() {
		if (this_element.getAttribute("width") < 0 || this_element.getAttribute("state") != "leave") 
		{
			clearInterval(id);
		}
		else
		{
			var width = this_element.getAttribute("width");
			width--;
			this_element.setAttribute("width", width);
			leftFillBar.style.width = (width * rating / 100 + 3) + '%';
			rightFillBar.style.width = (width * (100 - rating) / 100 + 3) + '%';
			rightFillBar.style.left = (100 - width * (100 - rating) / 100 + (3 * (100 - width) / 100) ) + '%'; 
		}
	}
}









function handlePaginationMove(pageNumber, maxPageNumber) {
	
	pageNumber =  Number(pageNumber);
	maxPageNumber = Number(maxPageNumber);
	
	
	document.getElementById("prevPageBtn").classList.remove("disabled");
	document.getElementById("startPageBtn").classList.remove("disabled");
	document.getElementById("nextPageBtn").classList.remove("disabled");
	document.getElementById("lastPageBtn").classList.remove("disabled");
	
	if(pageNumber == 1)
	{	
		document.getElementById("prevPageBtn").classList.add("disabled");
		document.getElementById("startPageBtn").classList.add("disabled");
	}
	
	if(pageNumber == maxPageNumber)
	{
		document.getElementById("nextPageBtn").classList.add("disabled");
		document.getElementById("lastPageBtn").classList.add("disabled");
	}	
	
	
	
	for(var i = 1; i < 6; ++i)
	{
		var elem = document.getElementById("pg" + i);;
		elem.classList.add("disabled");
	}
	
	
	for(var i = 1; i < (maxPageNumber > 5 ? 6 : maxPageNumber + 1); ++i)
	{
		var elem = document.getElementById("pg" + i);;
		elem.classList.remove("disabled");
	}
	
	
	
	
	
	if(pageNumber < 3)
	{
		for(var i = 1; i < 6; ++i)
		{
			var elem = document.getElementById("pg" + i);
				elem.childNodes[0].innerHTML = i;
				elem.classList.remove("active");
		}
		
		document.getElementById("pg" + pageNumber).classList.add("active");
	}
	else if(pageNumber > maxPageNumber - 3)
	{
		for(var i = 1; i < 6; ++i)
		{
			var elem = document.getElementById("pg" + i);
				elem.childNodes[0].innerHTML = maxPageNumber - 5 + i;
				elem.classList.remove("active");
		}
		
		console.log("-------->" + (5 + pageNumber - maxPageNumber) + "<--------------");
		document.getElementById("pg" + (5 + pageNumber - maxPageNumber)).classList.add("active");
	}
	else
	{
		for(var i = 1; i < 6; ++i)
		{
			var elem = document.getElementById("pg" + i);
				elem.childNodes[0].innerHTML = pageNumber - 3 + i;
				elem.classList.remove("active");
		}
		
		document.getElementById("pg3").classList.add("active");
	}
	
	
	clearUserList();
	generateUserList();
}





function nextPage() {

	var pagination = document.getElementsByClassName("pagination")[0];
	var userList = document.getElementById("userList");
	var pageNumber = userList.getAttribute("pageNumber");
	var paginationPageNumber = pagination.getAttribute("startPage");
	var maxPageNumber = Math.ceil(pagination.getAttribute("databaseUserCount") / 10);
	
	
	if(pageNumber < maxPageNumber)
	{
		pageNumber++;
		userList.setAttribute("pageNumber", pageNumber);
	}
	else
	{
		return;
	}
	
	
	
	console.log("pageNumber: ", pageNumber);
	console.log("paginationPageNumber:", paginationPageNumber);
	console.log("maxPageNumber: ", maxPageNumber);
	
	handlePaginationMove(pageNumber, maxPageNumber);
	
	
}

function prevPage() {

	var pagination = document.getElementsByClassName("pagination")[0];
	var userList = document.getElementById("userList");
	var pageNumber = userList.getAttribute("pageNumber");
	var paginationPageNumber = pagination.getAttribute("startPage");
	var maxPageNumber = Math.ceil(pagination.getAttribute("databaseUserCount") / 10);
	
	
	if(pageNumber > 1)
	{
		pageNumber--;
		userList.setAttribute("pageNumber", pageNumber);
	}
	else
	{
		return;
	}
	
	
	
	console.log("pageNumber: ", pageNumber);
	console.log("paginationPageNumber:", paginationPageNumber);
	console.log("maxPageNumber: ", maxPageNumber);
	
	handlePaginationMove(pageNumber, maxPageNumber);
	
}


function firstPage() {

	console.log("firstPage");
	
	var pagination = document.getElementsByClassName("pagination")[0];
	var userList = document.getElementById("userList");
	var pageNumber = userList.getAttribute("pageNumber");
	var paginationPageNumber = pagination.getAttribute("startPage");
	var maxPageNumber = Math.ceil(pagination.getAttribute("databaseUserCount") / 10);
	
	if(pageNumber != 1)
	{
		userList.setAttribute("pageNumber", 1);
		pageNumber = 1;
	}
	else
	{
		return;
	}
	
	console.log("pageNumber: ", pageNumber);
	console.log("paginationPageNumber:", paginationPageNumber);
	console.log("maxPageNumber: ", maxPageNumber);
	
	handlePaginationMove(pageNumber, maxPageNumber);
	
}


function lastPage() {

	var pagination = document.getElementsByClassName("pagination")[0];
	var userList = document.getElementById("userList");
	var pageNumber = userList.getAttribute("pageNumber");
	var paginationPageNumber = pagination.getAttribute("startPage");
	var maxPageNumber = Math.ceil(pagination.getAttribute("databaseUserCount") / 10);
	
	
	if(pageNumber != maxPageNumber)
	{
		userList.setAttribute("pageNumber", maxPageNumber);
		pageNumber = maxPageNumber;
	}
	else
	{
		return;
	}
	
	
	
	console.log("pageNumber: ", pageNumber);
	console.log("paginationPageNumber:", paginationPageNumber);
	console.log("maxPageNumber: ", maxPageNumber);
	
	handlePaginationMove(pageNumber, maxPageNumber);
	
}


	
function thisPage(this_elem) {

	var pagination = document.getElementsByClassName("pagination")[0];
	var userList = document.getElementById("userList");
	var pageNumber = userList.getAttribute("pageNumber");
	var newPageNumber = this_elem.childNodes[0].innerHTML;
	var paginationPageNumber = pagination.getAttribute("startPage");
	var maxPageNumber = Math.ceil(pagination.getAttribute("databaseUserCount") / 10);
	
	
	if(pageNumber != newPageNumber && newPageNumber <= maxPageNumber)
	{
		userList.setAttribute("pageNumber", newPageNumber);
		pageNumber = newPageNumber;
	}
	else
	{
		return;
	}
	
	
	
	console.log("pageNumber: ", pageNumber);
	console.log("paginationPageNumber:", paginationPageNumber);
	console.log("maxPageNumber: ", maxPageNumber);
	
	handlePaginationMove(pageNumber, maxPageNumber);
	
}

function htmlDecode(input){
	  var e = document.createElement('div');
	  e.innerHTML = input;
	  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
	}

function updateClipboard(newClip) {
	  console.log("To clipboard: ", htmlDecode(newClip));
	  navigator.clipboard.writeText(htmlDecode(newClip)).then(function() {
	    /* clipboard successfully set */
	  }, function() {
	    /* clipboard write failed */
	  });
}





function displayBlockImageModal(imageDiv) {
	var youtubeVideoID = imageDiv.getAttribute("youtubeVideoID");
	var modal = document.getElementById("imageModal");
	var postImageForm = document.getElementById("post-image-form");
	var hiddenYTVideoID = postImageForm.getElementsByClassName("hiddenVideoID");
	
	hiddenYTVideoID[0].setAttribute("value", youtubeVideoID);
	modal.style.display = "block";
	console.log("youtube video id: " + youtubeVideoID);
}


function displayNoneImageModal() {
	var modal = document.getElementById("imageModal");
	
	modal.style.display = "none";
}









function displayBlockTitleModal(titleDiv) {
	var youtubeVideoID = titleDiv.getAttribute("youtubeVideoID");
	var modal = document.getElementById("titleModal");
	var updateTitleForm = document.getElementById("update-title-form");
	var hiddenYTVideoID = updateTitleForm.getElementsByClassName("hiddenVideoID");
	
	hiddenYTVideoID[0].setAttribute("value", youtubeVideoID);
	modal.style.display = "block";
	console.log("youtube video id: " + youtubeVideoID);
}


function displayNoneTitleModal() {
	var modal = document.getElementById("titleModal");
	
	modal.style.display = "none";
}





function updateTitle(youtubeVideoID, title) {
	var titleDivs = null;
	
	titleDivs = document.getElementsByClassName("title");
	
	Array.prototype.forEach.call(titleDivs, function(element) 
		{
			if(element.getAttribute("youtubeVideoID").valueOf() == youtubeVideoID.valueOf())
			{
				element.innerHTML = title;
			}
		});
	
	
    console.log("Refreshed TITLE"); 
}

function removeVideoElement(youtubeVideoID) {

	var userList = document.getElementById("userList");
	var videoDivs = userList.children;
	
	Array.prototype.forEach.call(videoDivs, function(element) 
		{
			var videoID = element.getElementsByClassName("delete-video-style")[0].getAttribute("youtubeVideoID");
			
			if(videoID != null && videoID.valueOf() == youtubeVideoID.valueOf())
			{
				userList.removeChild(element);
			}
		});
	
	
    console.log("Refreshed TITLE"); 
}







function displayBlockInsertVideoModal(insertDiv) {
	var modal = document.getElementById("insertVideoModal");
	
	modal.style.display = "block";
}


function displayNoneInsertVideoModal() {
	var modal = document.getElementById("insertVideoModal");
	
	modal.style.display = "none";
}
















function displayNextRole(rolesListDiv) {
	
	var rolesDivs = rolesListDiv.children;
	
	for(var i = 0; i < rolesDivs.length; ++i)
	{
		if(!rolesDivs[i].classList.contains("notdisplay"))
		{
			rolesDivs[i].classList.remove("d-flex");
			rolesDivs[i].classList.add("notdisplay");
			
			i = i + 1;
			
			if(i == rolesDivs.length)
				i = 0;

			rolesDivs[i].classList.add("d-flex");
			rolesDivs[i].classList.remove("notdisplay");
			
			break;
		}
	}
}





function displayAddUserRoleModal(addRoleDiv) {
	var username = addRoleDiv.getAttribute("username");
	var modal = document.getElementById("addUserRoleModal");
	var form = document.getElementById("add-user-role-form");
	var hiddenUsername = form.getElementsByClassName("hiddenUsername")[0];
	
	hiddenUsername.setAttribute("value", username);
	modal.style.display = "block";
}



function displayNoneAddUserRoleModal() {
	var modal = document.getElementById("addUserRoleModal");
	
	modal.style.display = "none";
}





function addRole(username, roleTitle) {
	
	console.log("addRole(): start");
	
	var roleTemplate = document.getElementById("user-role-template");
	var userList = document.getElementById("userList");
	var userDivs = userList.children;
	
	console.log("addRolle(): roleTemplate = " + roleTemplate);
	console.log("addRolle(): userList = " + userList);
	console.log("addRolle(): userDivs = " + userDivs);
	
	
	
	Array.prototype.forEach.call(userDivs, function(element) 
	{
		console.log("addRolle(): element = " + userDivs);
		console.log(element);
		
		var elementUsername = element.getAttribute("username");
		
		console.log("addRolle(): elementUsername = " + elementUsername);
			
		if(elementUsername != null && elementUsername.valueOf() == username.valueOf())
		{
			var roleList = element.getElementsByClassName("roleList")[0];
			var newRoleItem = roleTemplate.cloneNode("true");
				
			newRoleItem.getElementsByClassName("role")[0].textContent = roleTitle;
			
			if(roleList.children.length == 0)
			{
				newRoleItem.classList.add("d-flex");
				newRoleItem.classList.remove("notdisplay");
			}
			
			roleList.appendChild(newRoleItem);
		}
	});
	
}


function deleteRole(username, roleTitle) {
	
	console.log("deleteRole(): start");
	
	var userList = document.getElementById("userList");
	var userDivs = userList.children;
	
	console.log("deleteRolle(): userList = " + userList);
	console.log("deleteRolle(): userDivs = " + userDivs);
	
	
	Array.prototype.forEach.call(userDivs, function(element) 
	{
		var elementUsername = element.getAttribute("username");
			
		if(elementUsername != null && elementUsername.valueOf() == username.valueOf())
		{
			var roleList = element.getElementsByClassName("roleList")[0];
			var rolesDivs = roleList.children;
			
			for(var i = 0; i < rolesDivs.length; ++i)
			{
				var currentRoleTitle = rolesDivs[i].getElementsByClassName("role")[0].textContent;
				
				if(currentRoleTitle.valueOf() == roleTitle.valueOf())
				{
					if(!rolesDivs[i].classList.contains("notdisplay"))
					{
						var j = i + 1;
						
						if(j == rolesDivs.length)
							j = 0;

						rolesDivs[j].classList.add("d-flex");
						rolesDivs[j].classList.remove("notdisplay");
					}
					
					roleList.removeChild(rolesDivs[i]);
					return;
				}
			}
			
			return;
		}
	});
	
}



function displayBlockAddUserModal() {
	var modal = document.getElementById("addUserModal");
	
	modal.style.display = "block";
	console.log("youtube video id: " + youtubeVideoID);
}


function displayNoneAddUserModal() {
	var modal = document.getElementById("addUserModal");
	
	modal.style.display = "none";
}






function displayBlockChangePasswordModal(changePasswordDiv) {
	
	var username = changePasswordDiv.getAttribute("username");
	var modal = document.getElementById("changePasswordModal");
	var form = document.getElementById("change-password-form");
	var hiddenUsername = form.getElementsByClassName("hiddenUsername")[0];
	
	hiddenUsername.setAttribute("value", username);
	
	modal.style.display = "block";
}





function displayNoneChangePasswordModal() {
	var modal = document.getElementById("changePasswordModal");
	
	modal.style.display = "none";
}