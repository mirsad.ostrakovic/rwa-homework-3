var APIkey = "AIzaSyD-8Rv2NLpVsceAHSQ7kwwV3oAzDUNkUlA";


function fetchJSONFile(path, callback) {
	var httpRequest = new XMLHttpRequest();
	
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200) {
				var data = JSON.parse(httpRequest.responseText);
				if (callback) callback(data);
			}
		}
	};
	
	httpRequest.open('GET', path);
	httpRequest.send(); 
} 


 
 
function clearRankingList() {
	var rankingList = document.getElementById("rankingList");

	while (rankingList.firstChild) {
		rankingList.removeChild(rankingList.firstChild);
	}
}
 
 
 
 function generateRankingList() {
	
	 	var rankingTemplate = document.getElementById("ranking-template");
		var rankingList = document.getElementById("rankingList");
		var pageNumber = rankingList.getAttribute("pageNumber");
		var URL = "http://localhost:8080/RWA_HOMEWORK_2/GetVideoByRating?startPosition=" + (pageNumber-1) * 10 + "&maxVideoCount=10";
		
		fetchJSONFile(URL, function(data) {
					 
			
			var arrayLength = data.length;
					 
			var videoYTIDs = [];
					 
			for(var i = 0 ; i < arrayLength; ++i)
			{
				var video = data[i];
				videoYTIDs.push(video.videoYoutubeID);
			}
					 
			getVideoInfo(videoYTIDs).then(function(result) {
						 
				var videoInfo = result;
						 
				for (var i = 0; i < arrayLength; i++) 
				{
					var video = data[i];
					
					var title = null;
					
					if ('title' in video)
						title = video.title;
					else
						title = videoInfo.find(function(element) { return element.id == video.videoYoutubeID; }).title;
							 
					var newRankingListItem = rankingTemplate.cloneNode("true");
							 
					newRankingListItem.classList.remove("notdisplay");		 
							
					newRankingListItem.getElementsByClassName("black-glossy-video-stat")[0].setAttribute("rating", Math.round(video.videoScore * 100));		 
					
					newRankingListItem.getElementsByClassName("title")[0].textContent = title;//title;
					
					// for administration purpose
					
					newRankingListItem.getElementsByClassName("circle-img-div")[0].setAttribute("youtubeVideoID", video.videoYoutubeID);
					
					newRankingListItem.getElementsByClassName("title")[0].setAttribute("youtubeVideoID", video.videoYoutubeID);
					
					newRankingListItem.getElementsByClassName("delete-video-style")[0].setAttribute("youtubeVideoID", video.videoYoutubeID);
					
					
					
					if(video.hasVideoImage)
						newRankingListItem.getElementsByClassName("round-video-img")[0].setAttribute("src", "../../GetImage?id=" + video.videoYoutubeID);
					else
						newRankingListItem.getElementsByClassName("round-video-img")[0].setAttribute("src", "https://img.youtube.com/vi/" + video.videoYoutubeID + "/hqdefault.jpg");
							 
					newRankingListItem.getElementsByClassName("video-stat-text")[0].textContent = video.videoLikes + "/" + video.videoDislikes;
				
					//newRankingListItem.getElementsByClassName("black-glossy-rank")[0].textContent = (pageNumber-1) * 10 + i + 1;
							 
					rankingList.appendChild(newRankingListItem);
				}						 
						 				 
		});;
	});
}
 
 
 
 

function getVideoCountDB() {
	 
	 URL = "http://localhost:8080/RWA_HOMEWORK_2/GetVideoCount";
	 
	 fetchJSONFile(URL, function(data) {
		 
			var paginationDiv = document.getElementsByClassName("pagination")[0];
			paginationDiv.setAttribute("databaseVideoCount", data);
			
			console.log("firstPage");
			firstPage();
			console.log("Database video count: ", data);
	 });
}




function initClientRankingLoad() {
	 gapi.client.setApiKey(APIkey);
	 gapi.client.load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest").then(function() {
		 	generateRankingList();
		 	getVideoCountDB();
	 });
}



function handleClientRankingLoad() {
	gapi.load('client', initClientRankingLoad);	
}




function getVideoInfo(videoIDs) {
	 
	var idParams = "";
	var retValue = [];

	for(var id in videoIDs)	
		idParams += videoIDs[id] + ','; 

	idParams = idParams.substring(0, idParams.length - 1);


	return gapi.client.youtube.videos.list({
   			"part": "snippet",
   			"id": idParams
   	})
   	.then(function(response) {
               
   			var items = response.result.items;

			items.forEach(function(elem){
   				retValue.push({id: elem.id, title: elem.snippet.localized.title});
   			});
   			
   			return retValue;
   		},
   		function(err) { console.error("Execute error", err); });
 }


function reload(youtubeVideoID){
	var circleDivs = null;
	var content = null;
	
	circleDivs = document.getElementsByClassName("circle-img-div");
	
	Array.prototype.forEach.call(circleDivs, function(element) 
		{
			if(element.getAttribute("youtubeVideoID").valueOf() == youtubeVideoID.valueOf())
			{
				console.log(element.children[0]);
				console.log(element.children[0].getAttribute("src"));
				
				var oldSrc = element.children[0].getAttribute("src");
				var newSrc = null;
				
				var addPartIdx = oldSrc.search("[\?]");
				
				if(addPartIdx == -1)
					newSrc = oldSrc;
				else 
					newSrc = oldSrc.slice(0, addPartIdx);
				
				console.log("OldSrc = " + oldSrc);
				console.log("NewSrc = " + newSrc);

				//if(video.hasVideoImage)
				//	newRankingListItem.getElementsByClassName("round-video-img")[0].setAttribute("src", "../../GetImage?id=" + video.videoYoutubeID);
				//else
				//	newRankingListItem.getElementsByClassName("round-video-img")[0].setAttribute("src", "https://img.youtube.com/vi/" + video.videoYoutubeID + "/hqdefault.jpg");
				
				element.children[0].setAttribute("src", "../../GetImage?id=" + youtubeVideoID + "&random=" + new Date().getTime());
			}
		});
	
	
    console.log("Refreshed"); 
}

function deleteVideo(deleteButton){
	var youtubeVideoID = deleteButton.getAttribute("youtubeVideoID");
	
	 $.ajax({
	        url: '../video?id=' + youtubeVideoID,
	        type: 'DELETE',
	        data:  null,
	        async: false,
	        cache: false,
	        contentType: false,
	        processData: false,
	        success: function (returndata) {
	            //$("#productFormOutput").html(returndata);
	            alert("Successful delete");
	            removeVideoElement(youtubeVideoID);
	        },
	        error: function () {
	            alert("error in ajax form submission");
	        }
	    });
	
	console.log("youtube video id: " + youtubeVideoID);
}


