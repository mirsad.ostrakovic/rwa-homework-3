package service;

import javax.activation.MimetypesFileTypeMap;
import javax.persistence.*;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;


import model.Role;
import model.User;
import model.Video;
import model.VideoImage;

public class AdminService {
	
	
	public static boolean changeUserPassword(String username, String password)
	{
		User user = null;
		Role role = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			user.changePassword(password);
			
			em.getTransaction().begin();
			em.merge(user);
			em.getTransaction().commit();
			
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;
	}
	
	
	
	public static boolean deleteUser(String username) 
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			System.out.println("AdminService.deleteUserRole(): user != null");
			
			
			em.getTransaction().begin();
			em.remove(user);
			em.getTransaction().commit();
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;
		
	}
	
	public static boolean addUser(String username, String password) 
	{
		User user = null;
		Role role = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() != 0)
				return false;
			
			user = new User(username, password);
			
			em.getTransaction().begin();		
			em.persist(user);
			em.getTransaction().commit();
			
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;

	}
	
	
	
	
	
	
	
	
	public static boolean deleteUserRole(String username, String roleTitle)
	{
		User user = null;
		Role role = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			System.out.println("AdminService.deleteUserRole(): user != null");
			
			TypedQuery<Role> getRoleQuery = em.createNamedQuery("Role.getRoleByRoleTitle", Role.class);
			
			getRoleQuery.setParameter("roleTitle", roleTitle);
			List<Role> roleList = getRoleQuery.getResultList();
			
			if(roleList.size() == 0)
				return false;
			
			role = roleList.get(0);
			
			System.out.println("AdminService.deleteUserRole(): role != null");
		
			
			if(!user.getUserRolesCollection().contains(role))
				return false;
			
			
			System.out.println("AdminService.deleteUserRole(): user play 'role'");
			
			
			em.getTransaction().begin();
			
			user.removeRole(role);
			
			em.merge(user);
			em.merge(role);
		
			em.getTransaction().commit();
			
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	public static boolean addUserRole(String username, String roleTitle)
	{
		
		User user = null;
		Role role = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			
			
			TypedQuery<Role> getRoleQuery = em.createNamedQuery("Role.getRoleByRoleTitle", Role.class);
			
			getRoleQuery.setParameter("roleTitle", roleTitle);
			List<Role> roleList = getRoleQuery.getResultList();
			
			if(roleList.size() == 0)
				return false;
			
			role = roleList.get(0);
			
			
			
			
			if(user.getUserRolesCollection().contains(role))
				return false;
			
			
			
			em.getTransaction().begin();
			
			user.addRole(role);
			
			em.merge(user);
			em.merge(role);
		
			em.getTransaction().commit();
			
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;

	}

	
	
	
	
	
	
	public static Long getUserCountDB()
	{
		try {
	   
			EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
			TypedQuery<Long> userCountQuery = em.createNamedQuery("User.getUserCount", Long.class);
			Long userCount = userCountQuery.getSingleResult();
			
			em.close();
			
			return userCount;
		
		} catch (Throwable t) {
		    System.out.println("AdminService.getUserCountDB()");
		    throw t;
		}
		
	}
	
	
	

	public static List<User> getUsers(Integer startResult, Integer maxCount)
	{
		try {
			   
			EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getAll", User.class);
			getUserQuery.setMaxResults(maxCount);
			getUserQuery.setFirstResult(startResult);
			List<User> users = getUserQuery.getResultList();
		
			em.close();
			
			return users;
		
		} catch (Throwable t) {
		    System.out.println("AdminService.getUsers()");
		    throw t;
		}
		
	}
	
	
	
	
	
	
	
	public static boolean checkAddUserPermission(String username) 
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		System.out.println("AdminService.checkAddUserPerm()");
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			Collection<Role> userRoles =  user.getUserRolesCollection();
        	 	
        	for(Role r : userRoles)
        		if(r.addUserPerm)
        			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return false;
	}
	
	
	
	
	public static boolean checkGetUserPermission(String username) 
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		System.out.println("AdminService.checkEditUserPerm()");
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			Collection<Role> userRoles =  user.getUserRolesCollection();
        	 	
        	for(Role r : userRoles)
        		if(r.getUserPerm)
        			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static boolean checkEditUserPermission(String username) 
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		System.out.println("AdminService.checkEditUserPerm()");
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			Collection<Role> userRoles =  user.getUserRolesCollection();
        	 	
        	for(Role r : userRoles)
        		if(r.editUserPerm)
        			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public static boolean checkDeleteUserPermission(String username) 
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		System.out.println("AdminService.checkDeleteUserPerm()");
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			Collection<Role> userRoles =  user.getUserRolesCollection();
        	 	
        	for(Role r : userRoles)
        		if(r.deleteUserPerm)
        			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return false;
	}
	
	
	
	
	
	
	public static boolean checkAddVideoPermission(String username)
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			Collection<Role> userRoles =  user.getUserRolesCollection();
        	 	
        	for(Role r : userRoles)
        		if(r.addVideoPerm)
        			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return false;
	}
	
	
	
	public static boolean checkDeleteVideoPermission(String username)
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			
			Collection<Role> userRoles =  user.getUserRolesCollection();
    	 	
        	for(Role r : userRoles)
        		if(r.deleteVideoPerm)
        			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return false;
	}
	
	
	
	
	public static boolean checkEditVideoPermission(String username)
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			Collection<Role> userRoles =  user.getUserRolesCollection();
    	 	
        	for(Role r : userRoles)
        		if(r.editVideoPerm)
        			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return false;
	}
	
	
	
	
	public static boolean checkGetVideoPermission(String username)
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return false;
			
			user = userList.get(0);
			
			Collection<Role> userRoles =  user.getUserRolesCollection();
    	 	
        	for(Role r : userRoles)
        		if(r.getVideoPerm)
        			return true;
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static boolean checkUsername(String username)
	{
		return true;
	}
	
	
	
	public static boolean insertVideo(String videoYoutubeID, String mimeType, byte[] contentRaw, String title)
	{
	EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			
			TypedQuery<Video> getVideoQuery = em.createNamedQuery("Video.getVideoByYoutubeID", Video.class);
			
			getVideoQuery.setParameter("youtubeID", videoYoutubeID);
			List<Video> videoList = getVideoQuery.getResultList();
			
			if(videoList.size() != 0)
				return false;
			
			
			VideoImage videoImage =  null;
			Video newVideo = new Video(videoYoutubeID, 0l, 0l);
			
			if(title != null && title.length() != 0)
				newVideo.setVideoTitle(title);
			
			if(contentRaw != null & mimeType != null)
			{
				videoImage = new VideoImage(newVideo, mimeType, contentRaw);
				newVideo.setVideoImage(videoImage);
			}

			
			em.getTransaction().begin();
			
			em.persist(newVideo);
			if(videoImage != null)
				em.persist(videoImage);
			
			em.getTransaction().commit();
			
			return true;
		}
		catch(Exception e)
		{	
			return false;
		}
		
	}
	
	
	
	
	public static boolean deleteVideo(String videoYoutubeID)
	{
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		try {
			TypedQuery<Video> deleteVideoQuery = em.createNamedQuery("Video.deleteVideoByYoutubeID", Video.class);
			
			deleteVideoQuery.setParameter("youtubeID", videoYoutubeID);
			
			em.getTransaction().begin();
			deleteVideoQuery.executeUpdate();
			em.getTransaction().commit();
			
			return true;
		}
		catch(Exception e)
		{	
			return false;
		}
	}
	
	
	public static User authenticateUser(String username, String password)
	{
		User user = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		
		try {
			TypedQuery<User> getUserQuery = em.createNamedQuery("User.getUserByUsername", User.class);
			
			getUserQuery.setParameter("username", username);
			List<User> userList = getUserQuery.getResultList();
			
			if(userList.size() == 0)
				return null;
			
			user = userList.get(0);
					
			if(user.checkPassword(password))
				return user;
			else
				return null;	
		}
		catch(Exception e)
		{	
			return null;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public static Boolean updateVideoTitle(String videoYoutubeID, String title)
	{
		Video video = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		
		if(title.length() == 0)
			title = null;
		
		
		try {
			TypedQuery<Video> getVideoQuery = em.createNamedQuery("Video.getVideoByYoutubeID", Video.class);
			
			getVideoQuery.setParameter("youtubeID", videoYoutubeID);
			List<Video> videoList = getVideoQuery.getResultList();
			
			if(videoList.size() == 0)
				return false;
			
			video = videoList.get(0);
					
			video.setVideoTitle(title);
			
			em.getTransaction().begin();
			em.persist(video);
			em.getTransaction().commit();
			
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;
	}
	
	
	
	
	
	
	
	public static Boolean postVideoImage(String youtubeVideoID, String mimeType, byte[] contentRaw)
	{
		Video video = null;
		EntityManager em = PersistenceContextHandler.getEMF().createEntityManager();
		
		
		try {
			TypedQuery<Video> getVideoQuery = em.createNamedQuery("Video.getVideoByYoutubeID", Video.class);
			
			getVideoQuery.setParameter("youtubeID", youtubeVideoID);
			List<Video> videoList = getVideoQuery.getResultList();
			
			if(videoList.size() == 0)
				return false;
			
			video = videoList.get(0);
		
			
			
			VideoImage videoImage = new VideoImage(video, mimeType, contentRaw);
			video.setVideoImage(videoImage);
			
			em.getTransaction().begin();
			em.persist(videoImage);
			em.persist(video);
			em.getTransaction().commit();
			
			
		}
		catch(Exception e)
		{	
			return false;
		}
		
		return true;
	}
}
