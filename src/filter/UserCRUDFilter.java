package filter;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Role;
import model.User;
import service.AdminService;

@WebFilter("/admin/user/*")
public class UserCRUDFilter implements Filter {
	
	public UserCRUDFilter() {}

	public void destroy() {}

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException 
    {   
    	System.out.println("UserCRUDFilter");
    	
      	HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String loginURI = request.getContextPath() + "/login.jsp";
        
        
        if(session == null || session.getAttribute("user") == null)
        {
        	response.sendError(HttpServletResponse.SC_FORBIDDEN);
        	return;
        }
        
        User user = (User)session.getAttribute("user");
        
        
        if(request.getMethod().equals("GET"))
        {
        	if(AdminService.checkGetUserPermission(user.getUsername()))
        	{
        		chain.doFilter(request, response);
        		return;
        	}
        }
        else if(request.getMethod().equals("POST"))
        {
        	System.out.println("Content path: " + request.getServletPath());
        	String[] pathParts = request.getServletPath().split("/");
        	
        	for(int i = 0; i < pathParts.length; ++i)
        		if(pathParts[i].equals("user"))
        		{
        			
        			if( i + 1 != pathParts.length && (pathParts[i+1].equals("password") || pathParts[i+1].equals("role")) )
        			{
        				System.out.println("EDIT USER PERMISSION CHECK");
        				if(AdminService.checkEditUserPermission(user.getUsername()))
                    	{
                    		chain.doFilter(request, response);
                    		return;
                    	}
        				else
        					break;
        			}
        			else
        			{
        				if(AdminService.checkAddUserPermission(user.getUsername()))
                    	{
                    		chain.doFilter(request, response);
                    		return;
                    	}
        				else
        					break;
        			}
        		}
        	
        }
        else if(request.getMethod().equals("DELETE"))
        {
        	
        	System.out.println("Content path: " + request.getServletPath());
        	String[] pathParts = request.getServletPath().split("/");
        	
        	for(int i = 0; i < pathParts.length; ++i)
        		if(pathParts[i].equals("user"))
        		{
        			
        			if( i + 1 != pathParts.length && (pathParts[i+1].equals("role")) )
        			{
        				System.out.println("EDIT USER PERMISSION CHECK");
        				if(AdminService.checkEditUserPermission(user.getUsername()))
                    	{
                    		chain.doFilter(request, response);
                    		return;
                    	}
        				else
        					break;
        			}
        			else
        			{
        				if(AdminService.checkDeleteUserPermission(user.getUsername()))
                    	{
                    		chain.doFilter(request, response);
                    		return;
                    	}
        				else
        					break;
        			}
        		}
        }
        
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
   
    }

	public void init(FilterConfig fConfig) throws ServletException {}

}
