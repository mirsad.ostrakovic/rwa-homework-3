package filter;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Role;
import model.User;
import service.AdminService;

@WebFilter("/admin/video/*")
public class VideoCRUDFilter implements Filter {
	
	public VideoCRUDFilter() {}

	public void destroy() {}

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException 
    {   
    	System.out.println("VideoCRUDFilter");
    	
      	HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String loginURI = request.getContextPath() + "/login.jsp";
        
        
        if(session == null || session.getAttribute("user") == null)
        {
        	response.sendError(HttpServletResponse.SC_FORBIDDEN);
        	return;
        }
        
        User user = (User)session.getAttribute("user");
        
        if(request.getMethod().equals("GET"))
        {
        	if(AdminService.checkGetVideoPermission(user.getUsername()))
        	{
        		chain.doFilter(request, response);
        		return;
        	}
        }
        else if(request.getMethod().equals("POST"))
        {
        	System.out.println("Content path: " + request.getServletPath());
        	String[] pathParts = request.getServletPath().split("/");
        	
        	for(int i = 0; i < pathParts.length; ++i)
        		if(pathParts[i].equals("video"))
        		{
        			
        			if( i + 1 != pathParts.length && (pathParts[i+1].equals("title") || pathParts[i+1].equals("image")) )
        			{
        				System.out.println("EDIT VIDEO PERMISSION CHECK");
        				if(AdminService.checkEditVideoPermission(user.getUsername()))
                    	{
                    		chain.doFilter(request, response);
                    		return;
                    	}
        				else
        					break;
        			}
        			else
        			{
        				if(AdminService.checkAddVideoPermission(user.getUsername()))
                    	{
                    		chain.doFilter(request, response);
                    		return;
                    	}
        				else
        					break;
        			}
        		}
        	
        }
        else if(request.getMethod().equals("DELETE"))
        {
        	if(AdminService.checkDeleteVideoPermission(user.getUsername()))
        	{
        		chain.doFilter(request, response);
        		return;
        	}
        }
        
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
   
    }

	public void init(FilterConfig fConfig) throws ServletException {}

}
