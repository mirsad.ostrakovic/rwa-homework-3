package servlet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;

import javax.persistence.TypedQuery;
import javax.servlet.*;
import javax.servlet.http.*;

import java.util.Arrays;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;

import service.AdminService;
import service.VideoService;
import model.Video;
import model.VideoImage;
import model.VideoPair;

@WebServlet("/admin/user/password")
public class ChangeUserPassword extends HttpServlet {

	public ChangeUserPassword() {
		super();
	}
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String username = null;
		String password = null;
		
		if(ServletFileUpload.isMultipartContent(request))
		{
			try 
			{
				List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(new ServletRequestContext(request));
	               
	            for(FileItem item : multiparts)
	            	if(item.isFormField())
	            	{
	            		System.out.println("ChangeUserPassword.doPost(): " + item.getFieldName() + " " + item.getString());
	            		if(username == null && item.getFieldName().equals("username"))
	            			username = item.getString();
	            		else if(password == null && item.getFieldName().equals("password"))
	            			password = item.getString();
	            	}
	        } 
			catch (Exception ex) 
			{
				System.out.println("ChangeUserPassword.doPost(): exception throwed");
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return;
	        }          
	          
	    }
		else
		{
			System.out.println("ChangeUserPassword.doPost(): not mutlipart content");
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		
				
		if(username == null || password == null)
		{
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		if(!AdminService.changeUserPassword(username, password))
		{
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		
		response.sendError(HttpServletResponse.SC_OK);	      
	}

	

}