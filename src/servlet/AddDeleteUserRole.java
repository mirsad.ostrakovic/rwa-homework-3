package servlet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;

import javax.persistence.TypedQuery;
import javax.servlet.*;
import javax.servlet.http.*;

import java.util.Arrays;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;

import service.AdminService;
import service.VideoService;
import model.Video;
import model.VideoImage;
import model.VideoPair;

@WebServlet("/admin/user/role")
public class AddDeleteUserRole extends HttpServlet {

	public AddDeleteUserRole() {
		super();
	}
	
	
	
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String username = request.getParameter("username");
		String roleTitle = request.getParameter("roleTitle");

		if(username == null || roleTitle == null)
		{
			System.out.println("AddDeleteUserRole.doDelete(): params is null");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		
		if(!AdminService.deleteUserRole(username, roleTitle))
		{
			System.out.println("AddDeleteUserRole.doDelete(): AdminService.deleteUserRole() problem");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		response.sendError(HttpServletResponse.SC_OK);	     	
	}
	
	
	
	
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String username = request.getParameter("username");
		String roleTitle = request.getParameter("roleTitle");
		
		if(username == null || roleTitle == null)
		{
			System.out.println("AddDeleteUserRole.doPost(): params is null");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		
		if(!AdminService.addUserRole(username, roleTitle))
		{
			System.out.println("AddDeleteUserRole.doPost(): AdminService.addUserRole() problem");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		
		response.sendError(HttpServletResponse.SC_OK);	      
	}

	

}