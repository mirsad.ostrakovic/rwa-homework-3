package servlet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;

import javax.persistence.TypedQuery;
import javax.servlet.*;
import javax.servlet.http.*;

import java.util.Arrays;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;

import service.AdminService;
import service.VideoService;
import model.Video;
import model.VideoImage;
import model.VideoPair;

@WebServlet("/admin/video/image")
public class PostImage extends HttpServlet {

	public PostImage() {
		super();
	}
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String youtubeVideoID = null;
		byte[] contentRaw = null;
		String contentType = null;
		
		System.out.println("PUT IMAGE CALLED");
		
		
		if(ServletFileUpload.isMultipartContent(request))
		{
			try 
			{
				List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(new ServletRequestContext(request));
	               
	            for(FileItem item : multiparts)
	            	if(item.isFormField())
	            	{
	            		if(youtubeVideoID == null)
	            			youtubeVideoID = item.getString();
	            	}
	            	else
	            	{
	            		if(contentRaw == null)
	            		{
	            			contentRaw = item.get();
	            			contentType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(contentRaw));
	            			System.out.println("File: " + item.getName());
	            		}
	                }
	              
	        } 
			catch (Exception ex) 
			{
				System.out.println("Exception throwed");
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return;
	        }          
	          
	    }
		else
		{
			System.out.println("Content is not multipart");
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		System.out.println("Get Image complated");
		
		if(youtubeVideoID == null)
			System.out.println("youtubeVideoID is null");
		
		if(contentRaw == null)
			System.out.println("contentRaw is null");
		
		if(contentType == null)
			System.out.println("contentType is null");
		
		
		
			
				
		if(youtubeVideoID == null || contentRaw == null || contentType == null || !contentType.split("/")[0].equals("image"))
		{
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
			
		System.out.println("Check data compleated");
		
		if(!AdminService.postVideoImage(youtubeVideoID, contentType, contentRaw))
		{
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		System.out.println("Post Video Image compleated");
		
		response.sendError(HttpServletResponse.SC_OK);	      
	}

	

}