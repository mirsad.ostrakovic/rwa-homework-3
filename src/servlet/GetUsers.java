package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIConversion.User;

import service.AdminService;
import service.VideoService;
import model.VideoPair;
import model.Video;

@WebServlet("/admin/GetUsers")
public class GetUsers extends HttpServlet {

	public GetUsers() {
		super();
	}
	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String startPosString = request.getParameter("startPosition");
		String maxUsersCountString = request.getParameter("maxUsersCount");
		
		Integer startPos;
		Integer maxUserCount;
		
		if(startPosString == null || maxUsersCountString == null)
		{
			System.out.println("Invalid parametars");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		
		try {
			startPos = Integer.parseInt(startPosString);
			maxUserCount = Integer.parseInt(maxUsersCountString);
		}
		catch(Exception e)
		{
			System.out.println("Invalid parametars value, not integers");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		if(startPos < 0 || maxUserCount <= 0)
		{
			System.out.println("Invalid parametars value, need to be >= 0 and > 0");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		if(maxUserCount > 20)
			maxUserCount = 20;
		
		
		List<model.User> userList = AdminService.getUsers(startPos, maxUserCount);
		
		System.out.println("USER LIST LENGTH: " + userList.size());
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		
		if(userList != null)
			response.getWriter().write(gson.toJson(userList));
		else
			response.getWriter().write(gson.toJson("Error in DB communication"));
			
	}
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		doGet(request, response);
	}


}
