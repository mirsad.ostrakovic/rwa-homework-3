package servlet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;

import javax.persistence.TypedQuery;
import javax.servlet.*;
import javax.servlet.http.*;

import java.util.Arrays;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;

import service.AdminService;
import service.VideoService;
import model.Video;
import model.VideoImage;
import model.VideoPair;

@WebServlet("/admin/video")
public class  InsertDeleteVideo extends HttpServlet {

	public InsertDeleteVideo() {
		super();
	}
	
	
	
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String videoYoutubeID = request.getParameter("id");
		
		String reqData = request.getQueryString();
		
		System.out.println("DELETE VIDEO");
		System.out.println("youtubeVideoID: " + videoYoutubeID);
		
		if(!AdminService.deleteVideo(videoYoutubeID))
		{
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		response.sendError(HttpServletResponse.SC_OK);	     	
	}
	
	
	
	
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String title = null;
		String videoYoutubeID = null;
		byte[] contentRaw = null;
		String contentType = null;
		
		System.out.println("INSERT VIDEO");
		
		
		if(ServletFileUpload.isMultipartContent(request))
		{
			try 
			{
				List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(new ServletRequestContext(request));
	               
	            for(FileItem item : multiparts)
	            	if(item.isFormField())
	            	{
	            		if(item.isFormField())
		            	{
		            		System.out.println("FormField: " + item.getFieldName() + " " + item.getString());
		            		if(title == null && item.getFieldName().equals("title"))
		            			title = item.getString();
		            		else if(videoYoutubeID == null && item.getFieldName().equals("youtubeVideoID"))
		            			videoYoutubeID = item.getString();
		            	}
	            	}
	            	else
	            	{
	            		if(contentRaw == null)
	            		{
	            			contentRaw = item.get();
	            			contentType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(contentRaw));
	            			System.out.println("File: " + item.getName());
	            		}
	                }
	              
	        } 
			catch (Exception ex) 
			{
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return;
	        }          
	          
	    }
		else
		{
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		System.out.println("Get video data complated");
		
		
		if(title == null)
			System.out.println("title is null");
			
		if(videoYoutubeID == null)
			System.out.println("youtubeVideoID is null");
		
		if(contentRaw == null)
			System.out.println("contentRaw is null");
		
		if(contentType == null)
			System.out.println("contentType is null");
		
		
		
		if( videoYoutubeID == null || (contentRaw != null && contentType != null && !contentType.split("/")[0].equals("image")) )
		{
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
			
		System.out.println("Check data compleated");
		
		if(!AdminService.insertVideo(videoYoutubeID, contentType, contentRaw, title))
		{
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		System.out.println("Insert video compleated");
		
		response.sendError(HttpServletResponse.SC_OK);	      
	}

	

}