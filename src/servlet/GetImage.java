package servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.persistence.TypedQuery;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;
import service.VideoService;
import model.Video;
import model.VideoImage;
import model.VideoPair;

@WebServlet("/GetImage")
public class GetImage extends HttpServlet {

	public GetImage() {
		super();
	}
	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String videoYoutubeID = request.getParameter("id");
		
		if(videoYoutubeID == null)
		{
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		VideoImage videoImage = VideoService.getVideoImage(videoYoutubeID);
    		
    	if(videoImage != null)
    	{
    		response.setContentType(videoImage.getMimeType());
    		response.setContentLength((int)videoImage.getImage().length);
    		response.getOutputStream().write(videoImage.getImage());
    	}
    	else
    		response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		
	}
	

}