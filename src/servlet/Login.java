package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.User;
import service.AdminService;

@WebServlet("/Login")
public class Login extends HttpServlet {

	public Login() {
		super();
	}
	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		boolean isDataAvailable = true;
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		if(username == null)
		{
			request.setAttribute("username", new String("null"));
			isDataAvailable = false;
		}
		
		if(password == null)
		{
			request.setAttribute("password", new String("null"));
			isDataAvailable = false;
		}
		
		
		if(isDataAvailable)
		{
			if(AdminService.checkUsername(username))
			{	
				User user = AdminService.authenticateUser(username, password);
				
				if(user != null)
				{
					request.getSession().setAttribute("user", user);
					System.out.println("LOGIN SUCCESSFULLY");
					System.out.println("getContextPath(): " + request.getContextPath());
					response.sendRedirect(request.getContextPath() + "/admin/");
					return;
				}
				else
					request.setAttribute("password", new String("invalid"));
			}
			else
				request.setAttribute("username", new String("invalid"));
	
		}
		
		//In case of any error code execution thread will be end here
		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		doGet(request, response);
	}

}
