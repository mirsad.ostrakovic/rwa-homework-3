package servlet;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;

import javax.persistence.TypedQuery;
import javax.servlet.*;
import javax.servlet.http.*;

import java.util.Arrays;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;

import service.AdminService;
import service.VideoService;
import model.Video;
import model.VideoImage;
import model.VideoPair;

@WebServlet("/admin/video/title")
public class UpdateVideoTitle extends HttpServlet {

	public UpdateVideoTitle() {
		super();
	}
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String title = null;
		String videoYoutubeID = null;
		
		System.out.println("PUT TITLE CALLED");
		System.out.println("ContentType: " + request.getContentType());
		
		if(ServletFileUpload.isMultipartContent(request))
		{
			try 
			{
				List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(new ServletRequestContext(request));
	               
	            for(FileItem item : multiparts)
	            	if(item.isFormField())
	            	{
	            		System.out.println("UpdateVideoTitle(): " + item.getFieldName() + " " + item.getString());
	            		if(title == null && item.getFieldName().equals("title"))
	            			title = item.getString();
	            		else if(videoYoutubeID == null && item.getFieldName().equals("youtubeVideoID"))
	            			videoYoutubeID = item.getString();
	            	}
	        } 
			catch (Exception ex) 
			{
				System.out.println("Exception throwed");
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
				return;
	        }          
	          
	    }
		else
		{
			System.out.println("Not mutlipart content");
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		
				
		if(title == null || videoYoutubeID == null)
		{
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		System.out.println("TITLE DATA CHECK PASSED");
		
		if(!AdminService.updateVideoTitle(videoYoutubeID, title))
		{
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		
		response.sendError(HttpServletResponse.SC_OK);	      
	}

	

}