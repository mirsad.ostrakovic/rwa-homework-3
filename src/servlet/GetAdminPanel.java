package servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.persistence.TypedQuery;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;
import service.VideoService;
import model.Video;
import model.VideoImage;
import model.VideoPair;

@WebServlet("/admin/")
public class GetAdminPanel extends HttpServlet {

	public GetAdminPanel() {
		super();
	}
	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.sendRedirect(request.getContextPath() + "/admin/video/video.jsp");
	}
	

}