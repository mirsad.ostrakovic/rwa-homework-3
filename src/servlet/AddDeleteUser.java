package servlet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;

import javax.persistence.TypedQuery;
import javax.servlet.*;
import javax.servlet.http.*;

import java.util.Arrays;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import javax.servlet.annotation.WebServlet;

import com.google.gson.Gson;

import service.AdminService;
import service.VideoService;
import model.Video;
import model.VideoImage;
import model.VideoPair;

@WebServlet("/admin/user")
public class AddDeleteUser extends HttpServlet {

	public AddDeleteUser() {
		super();
	}
	
	
	
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String username = request.getParameter("username");
		
		if(username == null)
		{
			System.out.println("AddDeleteUser.doDelete(): params is null");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		
		if(!AdminService.deleteUser(username))
		{
			System.out.println("AddDeleteUser.doDelete(): AdminService.deleteUser() problem");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		response.sendError(HttpServletResponse.SC_OK);	     	
	}
	
	
	
	
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		if(username == null)
		{
			System.out.println("AddDeleteUser.doPost(): params is null");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		
		if(!AdminService.addUser(username, password))
		{
			System.out.println("AddDeleteUser.doPost(): AdminService.addUser() problem");
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return; 
		}
		
		
		response.sendError(HttpServletResponse.SC_OK);	      
	}

	

}