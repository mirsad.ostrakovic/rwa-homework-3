package model;

import javax.persistence.*;

import com.google.gson.annotations.Expose;



@Entity(name = "Video")
@Table(name = "VIDEO")
@NamedQueries({
	@NamedQuery(name = "Video.getVideoByYoutubeID",
				query = "SELECT v FROM Video v WHERE v.videoYoutubeID = :youtubeID"),
	
	@NamedQuery(name = "Video.getVideoCount",
				query = "SELECT COUNT(v) FROM Video v"),
	
	@NamedQuery(name = "Video.getAll",
				query = "SELECT v FROM Video v"),
	
	@NamedQuery(name = "Video.getAllByRank",
				query = "SELECT v FROM Video v ORDER BY v.videoScore DESC"),
	
	@NamedQuery(name = "Video.deleteVideoByYoutubeID",
				query = "DELETE FROM Video v WHERE v.videoYoutubeID = :youtubeID")

})
public class Video {

	@Id
	@GeneratedValue
	@Column(name = "VIDEO_DATABASE_ID")
	private Long videoDatabaseID;

	
	@Column(name = "VIDEO_YOUTUBE_ID", nullable = false, unique = true)
	@Expose
	private String videoYoutubeID;

	
	@Column(name = "VIDEO_LIKES", nullable = false)
	@Expose
	private Long videoLikes;
	
	
	@Column(name = "VIDEO_DISLIKES", nullable = false)
	@Expose
	private Long videoDislikes;
	
	
	@Column(name = "VIDEO_SCORE", nullable = false)
	@Expose
	private Double videoScore;
	
	
	@Column(name = "VIDEO_TITLE")
	@Expose
	private String title;
	
	
	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "VIDEO_IMAGE_DATABASE_ID")
	private VideoImage videoImage;
	
	
	@Column(name = "HAS_VIDEO_IMAGE", nullable = false)
	@Expose
	private Boolean hasVideoImage;

	
	
	
	
	public void setVideoTitle(String title) {
		this.title = title;
	}
	
	public String getVideoTitle() { 
		return title;
	}

	
	public void setVideoImage(VideoImage videoImage) {
		if(videoImage != null)
			hasVideoImage = true;
		else
			hasVideoImage = false;
		
		this.videoImage = videoImage;
	}
	
	public VideoImage getVideoImage() {
		return videoImage;
	}
	
	
	
	public void positiveVote() {
		++videoLikes;
		updateVideoScore();
	}
		
	public void negativeVote() {
		++videoDislikes;
		updateVideoScore();
	}

	public String getYoutubeID() {
		return videoYoutubeID;
	}
	
	
	
	
	public Video() {
		// provided because of the need of 'JPA'
	}
	
	public Video(String youtubeID, Long posVote, Long negVote) {
		videoYoutubeID = youtubeID;
		videoLikes = posVote < 0 ? 0 : posVote;
		videoDislikes = negVote < 0 ? 0 : negVote;
		updateVideoScore();
		title = null;
		videoImage = null;
		hasVideoImage = false;
	}
	
	
	public Video(String youtubeID) {
		videoYoutubeID = youtubeID;
		videoLikes = 0l;
		videoDislikes = 0l;
		title = null;
		videoImage = null;
		hasVideoImage = false;
	}
	
	
	private void updateVideoScore() {
		Double pos = videoLikes.doubleValue();
		Double neg = videoDislikes.doubleValue();
		Double tot = pos + neg;
		if(tot != 0)
			videoScore = ((pos + 1.9208) / tot - 1.96 * Math.sqrt(((pos * neg) / tot +  0.9604) / tot / tot))/(1 + 3.8416 / tot);
		else
			videoScore = 0.0;
	}
	
}
