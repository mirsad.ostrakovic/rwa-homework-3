package model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.*;

import com.google.gson.annotations.Expose;

@Entity(name = "Role")
@Table(name = "ROLE")
@NamedQueries({
	@NamedQuery(name = "Role.getRoleByRoleTitle",
			query = "SELECT r FROM Role r WHERE r.roleTitle = :roleTitle")
})
public class Role {
	
	@Id
	@GeneratedValue
	@Column(name = "ROLE_DATABASE_ID")
	private Long roleDatabaseID;
	
	/*
	@ManyToMany(mappedBy = "rolesCollection")
	private Collection<RolesRegistration> rolesRegistrationCollection = new ArrayList<>();
	*/
	@ManyToMany(mappedBy = "userRolesCollection")
	private Collection<User> rolesUserCollection = new ArrayList<>();
	
	
	@Column(name = "ROLE_TITLE", nullable = false)
	@Expose
	private String roleTitle;
	
	

	@Column(name = "GET_VIDEO_PERM", nullable = false)
	@Expose
	public boolean getVideoPerm;
	
	@Column(name = "ADD_VIDEO_PERM", nullable = false)
	@Expose
	public boolean addVideoPerm;
	
	@Column(name = "DELETE_VIDEO_PERM", nullable = false)
	@Expose
	public boolean deleteVideoPerm;
	
	@Column(name = "EDIT_VIDEO_PERM", nullable = false)
	@Expose
	public boolean editVideoPerm;
	
	
	

	@Column(name = "GET_USER_PERM", nullable = false)
	@Expose
	public boolean getUserPerm;
	
	@Column(name = "ADD_USER_PERM", nullable = false)
	@Expose
	public boolean addUserPerm;
	
	@Column(name = "DELETE_USER_PERM", nullable = false)
	@Expose
	public boolean deleteUserPerm;
	
	@Column(name = "EDIT_USER_PERM", nullable = false)
	@Expose
	public boolean editUserPerm;


	/*
	public Collection<RolesRegistration> getRolesRegistrationCollection() {
		return rolesRegistrationCollection;
	}
	*/
	
	public Collection<User> getRolesUserCollection() {
		return rolesUserCollection;
	}

	public String getRoleTitle() { return roleTitle; }
	
	
	// CONSTRUCTORS

	public Role() {
	} // provided because of the need of JPA

	public Role(String roleTitle) {
		this.roleTitle = roleTitle;
		addVideoPerm = false;
		deleteVideoPerm = false;
		editVideoPerm = false;
		addUserPerm = false;
		deleteUserPerm = false;
		editUserPerm = false;
	}
}
