package model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.*;

import org.mindrot.BCrypt;

import com.google.gson.annotations.Expose;

@Entity(name = "User")
@Table(name = "USER")
@NamedQueries({
	@NamedQuery(name = "User.getAll",
			query = "SELECT u FROM User u"),
	
	@NamedQuery(name = "User.getUserByUsername",
			query = "SELECT u FROM User u WHERE u.username = :username"),
	
	@NamedQuery(name = "User.getUserCount",
			query = "SELECT COUNT(u) FROM User u")
})
public class User {
	
	@Id
	@GeneratedValue
	@Column(name = "USER_DATABASE_ID")
	private Long userDatabaseID;

	
	@Column(name = "USERNAME", nullable = false, unique = true)
	@Expose
	private String username;
	
	
	@Column(name = "CRYPTED_PASSWORD", nullable = false)
	private String cryptedPassword;
	
	/*
	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST, fetch=FetchType.EAGER)
	@Expose
	private Collection<RolesRegistration> rolesRegistrationCollection = new ArrayList<>();
	*/
	
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch=FetchType.EAGER)
	@JoinTable(name = "USER_REGISTERED_ROLES",
			   joinColumns = @JoinColumn(name = "USER_DATABASE_ID", referencedColumnName = "USER_DATABASE_ID"),
			   inverseJoinColumns = @JoinColumn(name = "ROLE_DATABASE_ID"))
	@Expose
	private Collection<Role> userRolesCollection = new ArrayList<>();
	
	public boolean addRole(Role role) {

		if (userRolesCollection.contains(role))
			return true;
		
		userRolesCollection.add(role);
		role.getRolesUserCollection().add(this);
		
		return true;
	}

	
	public void removeRole(Role role) {
		role.getRolesUserCollection().remove(this);
		userRolesCollection.remove(role);
	}


	
	public Long getDatabaseID() { return userDatabaseID; }
	
	public Collection<Role> getUserRolesCollection() { return userRolesCollection; }

	public String getUsername() { return username; }
	
	public String getCryptedPassword() { return cryptedPassword; }	
	
	
	private String cryptPassword(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}
	
	public boolean checkPassword(String password) {
		return BCrypt.checkpw(password, cryptedPassword);
	}		
	
	
	public void changePassword(String password) {
		this.cryptedPassword = cryptPassword(password);
	}
	
	
	
	public User() {
		// provided because of the need of 'JPA'
	}
	
	
	public User(String username, String password) {
		this.username = username;
		this.cryptedPassword = cryptPassword(password);
	}
	
	
}
