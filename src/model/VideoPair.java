package model;

import com.google.gson.annotations.Expose;

public class VideoPair {

	@Expose
	private Video firstVideo;
	
	@Expose
	private Video secondVideo;
	
	
	
	public Video getFirstVideo() { return firstVideo; }
	public Video getSecondVideo() { return secondVideo; }
	
	
	
	public VideoPair(Video fv, Video sv)
	{
		firstVideo = fv;
		secondVideo = sv;
	}
	
}
