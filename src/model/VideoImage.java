package model;

import javax.persistence.*;

import com.google.gson.annotations.Expose;

@Entity(name = "VideoImage")
@Table(name = "VIDEO_IMAGE")
@NamedQueries({
})
public class VideoImage {

	@Id
	@GeneratedValue
	@Column(name = "VIDEO_IMAGE_DATABASE_ID")
	private Long videoImageDatabaseID;
	
	@OneToOne(mappedBy = "videoImage")
	private Video video;
	
	@Lob
	@Column(name="IMAGE", nullable = false)
	@Expose
	private byte[] image;
	
	@Column(name = "MIME_TYPE", nullable = false)
	@Expose
	private String mimeType;
	
	
	
	public VideoImage() {
		// provided because of the need of 'JPA'
	}
	
	public VideoImage(Video video, String mimeType, byte[] image)
	{
		this.video = video;
		this.image = image;
		this.mimeType = mimeType;
	}
	
	
	
	public byte[] getImage() { return image; }
	public Video getImageVideo() { return video; }
	public String getMimeType() { return mimeType; }
	
}
