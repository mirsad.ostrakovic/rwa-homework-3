package init;

import java.io.File;
import java.io.FileInputStream;
import java.util.Random;

import javax.activation.MimetypesFileTypeMap;
import javax.persistence.*;
import model.*;



public class InitDatabase1 {
	
	private static final String PERSISTENCE_UNIT_NAME = "RWA_HOMEWORK_3";
	private static final Long MAX_VIDEO_VOTES = 10000l;
	private static EntityManagerFactory emf;
	private static Random random = new Random();

	private static String videoIDs[] = { "S-sJp1FfG7Q", "Hm1YFszJWbQ", "gGSH_0o-D7g",
										 "2CGv2Ud-KoE", "82NBsCOQoi4", "hLWGlq_2qbk",
										 "7_tWN9Iei7Y", "YFD2PPAqNbw", "5-MT5zeY6CU",
										 "k_UOuSklNL4", "quxTnEEETbo", "3X9LvC9WkkQ",
										 "NF65d49Vqeg", "1W5BA0lDVLM", "cedoBlUvBlI",
										 "papuvlVeZg8", "o1tj2zJ2Wvg", "7kUNjyjEwJY"};
	
	
	public static void main(String[] args) 
	{
		emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		EntityManager em = emf.createEntityManager();

		
		
		// Add videos to the database
		
		em.getTransaction().begin();
		
		for(String videoID : videoIDs)
		{
			em.persist(new Video(videoID, 
					 			 Math.abs(random.nextLong() % MAX_VIDEO_VOTES), 
					 			 Math.abs(random.nextLong() % MAX_VIDEO_VOTES)));
		}
		
		
		em.getTransaction().commit();
		
		
		
		
		em.getTransaction().begin();
		
		try {
			TypedQuery<Video> getRandomVideoQuery = em.createNamedQuery("Video.getAll", Video.class);
			getRandomVideoQuery.setMaxResults(1);
			
			File file = new File("img/JBDFav300.png");
			byte[] picInBytes = new byte[(int) file.length()];
			FileInputStream fileInputStream = new FileInputStream(file);
			fileInputStream.read(picInBytes);
			fileInputStream.close();
			
			String mimeType = new MimetypesFileTypeMap().getContentType(file);
			
			Video video = getRandomVideoQuery.getSingleResult();
			
			VideoImage videoImage = new VideoImage(video, mimeType, picInBytes);
			video.setVideoImage(videoImage);
			
			em.persist(videoImage);
			em.persist(video);
			em.getTransaction().commit();
			
		}
		catch(Exception e)
		{
			em.getTransaction().rollback();
		}
		
		
		
		User mirsad = new User("mirsad", "abcd1234");
		Role admin = new Role("admin");
		Role moderator = new Role("moderator");
		
		admin.addVideoPerm = true;
		admin.editVideoPerm = true;
		admin.deleteVideoPerm = true;
		admin.getVideoPerm = true;
		
		admin.addUserPerm = true;
		admin.editUserPerm = true;
		admin.deleteUserPerm = true;
		admin.getUserPerm = true;
		
		moderator.addVideoPerm = true;
		moderator.editVideoPerm = true;
		moderator.deleteVideoPerm = true;
		moderator.getVideoPerm = true;
		
		mirsad.addRole(admin);
		mirsad.addRole(moderator);
		
		em.getTransaction().begin();
		
		em.persist(mirsad);
		em.persist(admin);
		em.persist(moderator);
		
		em.getTransaction().commit();
		
		
		
		
		
		
		
		
	
		
		
		
		
		em.getTransaction().begin();

		int firstVideoHash, secondVideoHash;
		Video firstVideo, secondVideo;
		
		TypedQuery<Long> videoCountQuery = em.createNamedQuery("Video.getVideoCount", Long.class);
		Long videoCount = videoCountQuery.getSingleResult();
		
		TypedQuery<Video> getRandomVideoQuery = em.createNamedQuery("Video.getAll", Video.class);
		getRandomVideoQuery.setMaxResults(1);
	  
		
		for(int i = 0; i < 20; ++i)
		{
			firstVideoHash = random.nextInt(Math.toIntExact(videoCount));
			while((secondVideoHash = random.nextInt(Math.toIntExact(videoCount))) == firstVideoHash);
		
			
			getRandomVideoQuery.setFirstResult(firstVideoHash);
			firstVideo = getRandomVideoQuery.getSingleResult();
			
			getRandomVideoQuery.setFirstResult(secondVideoHash);
			secondVideo = getRandomVideoQuery.getSingleResult();
			
			em.persist(new VideoShare(firstVideo, secondVideo, "Hellou Msg!"));
		}
		
		
		em.getTransaction().commit();
		
		
		
		
		
		em.close();
		emf.close();
	}

}
